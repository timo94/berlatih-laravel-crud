
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <ol class="breadcrumb float-sm-right">
                @php 
                $path = "";
                if ($breadcrumbs) {
                    echo '<li class="breadcrumb-item"><a href="/">Home</a></li>';
                } else {
                    echo '<li class="breadcrumb-item active">Home</li>';
                }
                @endphp

                @foreach ($breadcrumbs as $index => $breadcrumb)
                    @php 
                    $path = $path .'/'. $breadcrumb;
                    $capitalize = ucwords($breadcrumb);
                    if ($index == count($breadcrumbs)-1){
                        echo '<li class="breadcrumb-item active">'. $capitalize .'</li>';
                    } else {
                        echo '<li class="breadcrumb-item"><a href="'. $path .'">'. $capitalize .'</a></li>';
                    }
                    @endphp
                @endforeach
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>