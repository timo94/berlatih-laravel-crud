@extends('adminlte.master')

@section('content')
@if(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
@endif
<div class="card">
    <div class="card-header d-flex lign-items-center justify-content-between">
        <h4 class="card-title mr-auto my-auto">{{ $pertanyaan->title }}</h4>
        <a class="btn btn-primary mr-0" href="/pertanyaan/{{$pertanyaan->id}}/edit">Edit</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>{{ $pertanyaan->content }}</p>
    </div>
</div>
@endsection