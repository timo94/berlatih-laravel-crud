@extends('adminlte.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endpush

@section('content')
<div class="card">
    <div class="card-header d-flex align-items-center justify-content-between">
        <h3 class="card-title mr-auto my-auto">Daftar Pertanyaan</h3>
        <a class="btn btn-primary mr-0" href="/pertanyaan/create">Buat Pertanyaan Baru</a>
    </div>
    @if(session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @endif
    <!-- /.card-header -->
    <div class="card-body">
        <table id="daftar_pertanyaan" class="table table-condensed table-hover">
            <thead>                                    
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th style="width: 40px">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($pertanyaan as $key => $pertanyaan)
                    <tr style="transform: rotate(0);">
                        <td class="align-middle">{{ $key+1 }}</td>
                        <td class="align-middle">{{ $pertanyaan->title }}</td>
                        <td class="align-middle">{{ $pertanyaan->content }}</td>
                        <td style="display: flex;">
                            <a class="btn btn-primary btn-sm" href="/pertanyaan/{{ $pertanyaan->id }}">Show</a>
                            <a class="btn btn-secondary btn-sm mx-1" href="/pertanyaan/{{ $pertanyaan->id }}/edit">Edit</a>
                            <form action="/pertanyaan/{{ $pertanyaan->id }}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" align="center">
                            <p>Tidak ada Pertanyaan</p>
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@push('scripts')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
    $(function () {
        $("#daftar_pertanyaan").DataTable();
    });
    </script>
@endpush