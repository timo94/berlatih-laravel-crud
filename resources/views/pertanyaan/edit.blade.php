@extends('adminlte.master')


@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Question #{{$pertanyaan->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $pertanyaan->title) }}" placeholder="Question Title">
                @error('title')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Content</label>
                <textarea class="form-control" id="body" name="content" rows="3" placeholder="Your question ...">{{ old('content', $pertanyaan->content) }}</textarea>
                @error('content')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="attach">Attach file</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="attach" name="attachment" value="{{ old('attachment', '') }}" >
                        <label class="custom-file-label" for="attach">Choose file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <div class="row">
                <div class="mx-1">
                    <form action="/pertanyaan/{{ $pertanyaan->id }}" method="post">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
                <div class="mx-1">
                    <form action="/pertanyaan/{{ $pertanyaan->id }}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection