<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(Request $request){
        $breadcrumbs = array_filter(explode('/', request()->path()));
        $pertanyaan = DB::table('questions')->get();
        return view('pertanyaan.index', compact('breadcrumbs', 'pertanyaan'));
    }

    public function create(Request $request){
        $breadcrumbs = array_filter(explode('/', request()->path()));
        return view('pertanyaan.create', compact('breadcrumbs'));
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);
        $id = DB::table('questions')->insertGetId([
            'title'=>$request['title'], 
            'content'=>$request['content']
        ]);
        return redirect('/pertanyaan/'.$id)->with('success', 'Pertanyaan Berhasil Disimpan!');
    }

    public function show($id, Request $request){
        $breadcrumbs = array_filter(explode('/', request()->path()));
        $pertanyaan = DB::table('questions')->where('id', $id)->first();
        
        return view('pertanyaan.show', compact('breadcrumbs', 'pertanyaan'));
    }

    public function edit($id, Request $request){
        $breadcrumbs = array_filter(explode('/', request()->path()));
        $pertanyaan = DB::table('questions')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('breadcrumbs', 'pertanyaan'));
    }

    public function update($id, Request $request){
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);
        $query = DB::table('questions')->where('id', $id)->update([
            'title'=>$request['title'], 
            'content'=>$request['content']
        ]);
        return redirect('/pertanyaan/'.$id)->with('success', 'Pertanyaan sudah di update!');
    }

    public function destroy($id, Request $request){
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan #'. $id .' sudah di hapus!');
    }
}
