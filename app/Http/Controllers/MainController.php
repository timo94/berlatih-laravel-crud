<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function home(Request $request){
        $breadcrumbs = array_filter(explode('/', request()->path()));
        return view('index', ['breadcrumbs'=>$breadcrumbs]);
    }

    public function data_table(Request $request){
        $breadcrumbs = explode('/', request()->path());
        return view('data-tables.table', ['breadcrumbs'=>$breadcrumbs]);
    }

}


